package ru.foreg.testTaskKBInform.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.foreg.testTaskKBInform.exception.ResourceNotFoundException;
import ru.foreg.testTaskKBInform.model.Client;
import ru.foreg.testTaskKBInform.model.Deal;
import ru.foreg.testTaskKBInform.repository.ClientRepository;
import ru.foreg.testTaskKBInform.repository.DealRepository;
import ru.foreg.testTaskKBInform.repository.PositionRepository;

import javax.validation.Valid;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/")
public class QueryController {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private DealRepository dealRepository;
    // Находит клиентов с датой рождения меньше чем bdate
    // Пример использования
    // http://localhost:8080/clientsBdate?currentPage=0&bdate=1980-01-01
    @GetMapping("/clientsBdate")
    public Page<Client> clientsBdate(@RequestParam Integer currentPage, @RequestParam Date bdate) {
        Pageable limit = new PageRequest(currentPage,2);
        return clientRepository.findBybdateLessThan(bdate, limit);
    }
    // Сортирует сделки по возрастанию
    // Пример использования
    // http://localhost:8080/dealsBySumm?currentPage=0
    @GetMapping("/dealsBySumm")
    public Page<Deal> dealsBySumm(@RequestParam Integer currentPage) {
        Pageable limit = new PageRequest(currentPage,2);
        return dealRepository.findAllByOrderBySummAsc(limit);
    }
    // Возвращает ФИО сотрудника, его должность, ФИО клиента для завершенных сделок
    // Пример использования
    // http://localhost:8080/customSelect?currentPage=0
    @GetMapping("/customSelect")
    public Page<Object> customSelect(@RequestParam Integer currentPage) {
        Pageable limit = new PageRequest(currentPage,2);
        return dealRepository.findByCustomSelect(limit);
    }

}
