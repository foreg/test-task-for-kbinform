package ru.foreg.testTaskKBInform.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.foreg.testTaskKBInform.exception.ResourceNotFoundException;
import ru.foreg.testTaskKBInform.model.Deal;
import ru.foreg.testTaskKBInform.repository.DealRepository;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/")
public class DealController {
    @Autowired
    private DealRepository dealRepository;
    @GetMapping("/deals")
    public Page<Deal> getAllDeals(@RequestParam Integer currentPage) {
        Pageable limit = new PageRequest(currentPage,2);
        return dealRepository.findAll(limit);
    }
    @GetMapping("/deals/{id}")
    public ResponseEntity<Deal> getDealById(@PathVariable(value = "id") Integer dealId)
            throws ResourceNotFoundException {
        Deal deal = dealRepository.findById(dealId)
                .orElseThrow(() -> new ResourceNotFoundException("Deal not found for this id :: " + dealId));
        return ResponseEntity.ok().body(deal);
    }
    @PostMapping("/deals")
    public Deal createDeal(@Valid @RequestBody Deal deal) {
        return dealRepository.save(deal);
    }
    @PutMapping("/deals/{id}")
    public ResponseEntity<Deal> updateDeal(@PathVariable(value = "id") Integer dealId,
                                                   @Valid @RequestBody Deal dealDetails) throws ResourceNotFoundException {
        Deal deal = dealRepository.findById(dealId)
                .orElseThrow(() -> new ResourceNotFoundException("Deal not found for this id :: " + dealId));
        deal.setClientId(dealDetails.getClientId());
        deal.setDone(dealDetails.isDone());
        deal.setEmployeeId(dealDetails.getEmployeeId());
        deal.setSumm(dealDetails.getSumm());
        final Deal updatedDeal = dealRepository.save(deal);
        return ResponseEntity.ok(updatedDeal);
    }
    @DeleteMapping("/deals/{id}")
    public Map<String, Boolean> deleteDeal(@PathVariable(value = "id") Integer dealId)
            throws ResourceNotFoundException {
        Deal deal = dealRepository.findById(dealId)
                .orElseThrow(() -> new ResourceNotFoundException("Deal not found for this id :: " + dealId));
        dealRepository.delete(deal);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
