package ru.foreg.testTaskKBInform.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.foreg.testTaskKBInform.model.Position;
import ru.foreg.testTaskKBInform.repository.PositionRepository;
import  ru.foreg.testTaskKBInform.exception.ResourceNotFoundException;


@RestController
@RequestMapping("/")
public class PositionController {
    @Autowired
    private PositionRepository positionRepository;
    @GetMapping("/positions")
    public Page<Position> getAllPositions(@RequestParam Integer currentPage) {
        Pageable limit = new PageRequest(currentPage,2);
        return positionRepository.findAll(limit);
    }
    @GetMapping("/positions/{id}")
    public ResponseEntity<Position> getPositionById(@PathVariable(value = "id") Integer positionId)
            throws ResourceNotFoundException {
        Position position = positionRepository.findById(positionId)
                .orElseThrow(() -> new ResourceNotFoundException("Position not found for this id :: " + positionId));
        return ResponseEntity.ok().body(position);
    }
    @PostMapping("/positions")
    public Position createPosition(@Valid @RequestBody Position position) {
        return positionRepository.save(position);
    }
    @PutMapping("/positions/{id}")
    public ResponseEntity<Position> updatePosition(@PathVariable(value = "id") Integer positionId,
                                                   @Valid @RequestBody Position positionDetails) throws ResourceNotFoundException {
        Position position = positionRepository.findById(positionId)
                .orElseThrow(() -> new ResourceNotFoundException("Position not found for this id :: " + positionId));
        position.setName(positionDetails.getName());
        position.setSalary(positionDetails.getSalary());
        final Position updatedPosition = positionRepository.save(position);
        return ResponseEntity.ok(updatedPosition);
    }
    @DeleteMapping("/positions/{id}")
    public Map<String, Boolean> deletePosition(@PathVariable(value = "id") Integer positionId)
            throws ResourceNotFoundException {
        Position position = positionRepository.findById(positionId)
                .orElseThrow(() -> new ResourceNotFoundException("Position not found for this id :: " + positionId));
        positionRepository.delete(position);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
