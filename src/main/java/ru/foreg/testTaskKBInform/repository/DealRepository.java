package ru.foreg.testTaskKBInform.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.foreg.testTaskKBInform.model.Deal;

@Repository
public interface DealRepository extends JpaRepository<Deal, Integer>{
    /**
     * Сортирует сделки по возрастанию summ
     * @param pageable Лимит для пагинации
     * @return Страницу с результатами запроса
     */
    Page<Deal> findAllByOrderBySummAsc(Pageable pageable);

    /**
     * Выводит страницу с ФИО работника, его должностью, ФИО клиента и суммой для всех завершенных сделок
     * @param pageRequest Лимит для пагинации
     * @return Страницу с результатами запроса
     */
    @Query(value = "select emp.name as \"ФИО работника\", pos.name as \"Должность\", cl.name as \"ФИО клиента\", d.summ as \"Сумма\" from deal d " +
            "inner join client cl on d.client_id = cl.id " +
            "inner join employee emp on d.employee_id = emp.id " +
            "inner join \"position\" pos on emp.position_id = pos.id " +
            "where \"done\" = true", nativeQuery=true)
    Page<Object> findByCustomSelect(Pageable pageRequest);
}
