package ru.foreg.testTaskKBInform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import  ru.foreg.testTaskKBInform.model.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer>{

}
