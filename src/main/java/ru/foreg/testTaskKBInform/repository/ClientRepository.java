package ru.foreg.testTaskKBInform.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.foreg.testTaskKBInform.model.Client;

import java.sql.Date;

public interface ClientRepository extends JpaRepository<Client, Integer>{
    /**
     * Найти клиентов с датой рождения меньше чем bdate
     * @param bdate Дата рождения
     * @param pageable Лимит для пагинации
     * @return Текущую страницу с результатами запроса
     */
    Page<Client> findBybdateLessThan(Date bdate, Pageable pageable);
}
