package ru.foreg.testTaskKBInform.model;

import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;        //ФИО работника
    private int position_id;    //id должности, которую он занимает
    public Employee() {
    }
    public Employee(String name, Integer position_id) {
        this.name = name;
        this.position_id = position_id;
    }

    public long getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition_id() {
        return position_id;
    }

    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + position_id;
    }
}
