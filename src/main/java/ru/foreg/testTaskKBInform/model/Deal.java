package ru.foreg.testTaskKBInform.model;

import javax.persistence.*;
import java.sql.Date;

// Сущность сделка хранит в себе id работника, заключившего сделку;
// id клиента, с которым была заключена сделка;
// отметку о выполнении done и сумму сделки summ
@Entity
@Table(name = "deal")
public class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int clientId;
    private int employeeId;
    private boolean done;
    private int summ;
    public Deal() {
    }

    public Deal(int id, int clientId, int employeeId, boolean done, int summ) {
        this.id = id;
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.done = done;
        this.summ = summ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public int getSumm() {
        return summ;
    }

    public void setSumm(int summ) {
        this.summ = summ;
    }

    @Override
    public String toString() {
        return id + " " + clientId + " " + employeeId + " " + done+ " " + summ ;
    }
}
