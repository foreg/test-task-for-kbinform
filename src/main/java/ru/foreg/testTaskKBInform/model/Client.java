package ru.foreg.testTaskKBInform.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date bdate;     //Дата рождения
    private String name;    //ФИО клиента
    public Client() {
    }
    public Client(Date bdate, String name) {
        this.name = name;
        this.bdate = bdate;
    }

    public long getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Date getBdate() {
        return bdate;
    }

    public void setBdate(Date bdate) {
        this.bdate = bdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + bdate;
    }
}
