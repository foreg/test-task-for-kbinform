package ru.foreg.testTaskKBInform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTaskKbInformApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestTaskKbInformApplication.class, args);
	}

}

